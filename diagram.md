
```plantuml
CSV -> Parser
Parser -> InboundMapper 
InboundMapper -> Validation
Validation -> Elasticsearch
```

```plantuml
Elasticsearch -> OutboundMapper
OutboundMapper -> Exporter
Exporter -> CSVExporter
CSVExporter -> CSV
```

```plantuml
Kafka -> Deserializer
Deserializer -> Validation
Validation -> Elasticsearch
```

```plantuml
Elasticsearch -> Serializer
Serializer -> Kafka
```

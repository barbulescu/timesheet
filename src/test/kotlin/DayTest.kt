import kotlinx.datetime.toLocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class DayTest {

    @Test
    fun `convert day to string`() {
        val text =
            Day("2021-11-12".toLocalDate(), "08:00 - 12:00".toInterval(), "12:30 - 15:00".toInterval()).toString()

        assertThat(text).isEqualTo(
            """
            |2021-11-12
            |0800 - 1200
            |1230 - 1500""".trimMargin()
        )
    }

    @Test
    fun `convert string to day`() {
        val day = """
            |2021-11-12
            |0800 - 1200
            |1230 - 1500""".trimMargin().toDay()

        assertThat(day).isEqualTo(
            Day("2021-11-12".toLocalDate(), "08:00 - 12:00".toInterval(), "12:30 - 15:00".toInterval())
        )
    }

    @Test
    fun validation() {
        assertAll(
            { assertFalse { Day("2021-11-12".toLocalDate()).valid() } },
            { assertTrue { Day("2021-11-12".toLocalDate(), "08:00 - 12:00".toInterval()).valid() } }
        )
    }

    @Test
    fun totalMinutes() {
        val day = Day("2021-11-12".toLocalDate(), "08:00 - 12:00".toInterval(), "12:30 - 16:54".toInterval())
        assertThat(day.totalMinutes()).isEqualTo(504)
    }

    @Test
    fun normalize() {
        val day = Day("2021-11-12".toLocalDate(), "09:00 - 11:00".toInterval(), "13:34 - 14:54".toInterval())
        val expected = Day("2021-11-12".toLocalDate(), "08:00 - 12:00".toInterval(), "12:30 - 16:54".toInterval())
        assertThat(day.normalize()).isEqualTo(expected)
    }
}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class IntervalTest {

    @Test
    fun `string to interval`() {
        val interval = "1200 - 1230".toInterval()
        assertThat(interval)
            .isEqualTo(Interval("1200".toTime(), "1230".toTime()))
    }

    @Test
    fun `interval to string`() {
        val interval = Interval("1200".toTime(), "1230".toTime())
        assertThat(interval.toString())
            .isEqualTo("1200 - 1230")
    }
}
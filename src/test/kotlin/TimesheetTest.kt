import kotlinx.datetime.toLocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll

internal class TimesheetTest {

    @Test
    fun `string to timesheet`() {
        val timesheet = """
            |#2021-11-11
            |0800 - 1200
            |1230 - 1700
            |
            |#2021-11-12
            |0900 - 1600
        """.trimMargin().toTimesheet()

        val expected = Timesheet(
            Day("2021-11-11".toLocalDate(), "0800 - 1200".toInterval(), "1230 - 1700".toInterval()),
            Day("2021-11-12".toLocalDate(), "0900 - 1600".toInterval())
        )

        assertThat(timesheet)
            .isEqualTo(expected)
    }

    @Test
    fun `timesheet to string`() {
        val text = Timesheet(
            Day("2021-11-11".toLocalDate(), "0800 - 1200".toInterval(), "1230 - 1700".toInterval()),
            Day("2021-11-12".toLocalDate(), "0900 - 1600".toInterval())
        ).toString()
        val expected = """
            |#2021-11-11
            |0800 - 1200
            |1230 - 1700
            |
            |#2021-11-12
            |0900 - 1600
        """.trimMargin()

        assertThat(text)
            .isEqualTo(expected)
    }

    @Test
    fun normalize() {
        val initial = Timesheet(Day("2021-11-12".toLocalDate(), "0500 - 0700".toInterval()))
        val expected = Timesheet(Day.normalizedDay("2021-11-12".toLocalDate()))

        val (actual, delta) = initial.normalize()

        assertAll(
            { assertThat(actual).isEqualTo(expected) },
            { assertThat(delta).isEqualTo(384) }
        )
    }
}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertTrue

internal class TimeTest {
    @Test
    fun `string to time`() {
        assertThat("1200".toTime()).isEqualTo(Time(12, 0))
    }

    @Test
    fun `time to string`() {
        assertThat(Time(12, 0).toString()).isEqualTo("1200")
    }

    @Test
    fun `compare times`() {
        assertAll(
            { assertTrue { "1200".toTime() < "1300".toTime() } },
            { assertTrue { "1200".toTime() == "1200".toTime() } },
            { assertTrue { "1200".toTime() > "1100".toTime() } }
        )
    }

    @Test
    fun `minutes to`() {
        val minutes = "1210".toTime() - "1200".toTime()
        assertThat(minutes).isEqualTo(10)
    }
}
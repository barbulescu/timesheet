import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll

internal class ConversionsKtTest {
    @Test
    fun hoursToMinutesTest() {
        assertAll(
            { assertThat(hoursToMinutes(1.0)).isEqualTo(60) },
            { assertThat(hoursToMinutes(1.5)).isEqualTo(90) },
            { assertThat(hoursToMinutes(8.4)).isEqualTo(504) }
        )
    }
}
data class Interval(private val start: Time, private val end: Time) {
    val minutes: Long = end - start

    override fun toString(): String {
        return "$start - $end"
    }
}

fun String.toInterval(): Interval {
    val parts = this.split("\\s*-\\s*".toRegex())
    require(parts.size == 2)
    val start = parts[0].toTime()
    val end = parts[1].toTime()
    return Interval(start, end)
}
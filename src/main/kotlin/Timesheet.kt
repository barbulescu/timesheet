data class Timesheet(private val days: List<Day>) {

    constructor(vararg days: Day) : this(days.toList())

    fun normalize(): Pair<Timesheet, Long> {
        val minutesInitial = totalMinutes()

        val normalized = Timesheet(days
            .filterNot { it.isWeekend() }
            .map { it.normalize() }
        )

        val minutesAfter = normalized.totalMinutes()
        println("minutesAfter=$minutesAfter minutesInitial=$minutesInitial")
        val difference = minutesAfter - minutesInitial
        return normalized to difference
    }

    private fun totalMinutes() = days.sumOf { it.totalMinutes() }

    override fun toString(): String {
        return days.joinToString(separator = "\n\n") { "#${it}" }
    }
}

fun String.toTimesheet(): Timesheet {
    val days = this.splitToSequence("#")
        .filterNot(String::isBlank)
        .map(String::toDay)
        .toList()
    return Timesheet(days)
}

import java.time.LocalTime
import java.time.temporal.ChronoUnit.MINUTES

data class Time(private val localTime: LocalTime) : Comparable<Time> {
    constructor(hour: Int, minute: Int) : this(LocalTime.of(hour, minute))

    override fun toString(): String {
        return "${format(localTime.hour)}${format(localTime.minute)}"
    }

    private fun format(value: Int): String = String.format("%02d", value)

    override fun compareTo(other: Time): Int = localTime.compareTo(other.localTime)

    operator fun minus(time: Time) = MINUTES.between(time.localTime, localTime)
}

fun String.toTime(): Time {
    val hour = this.take(2).toInt()
    val minutes = this.takeLast(2).toInt()
    return Time(hour, minutes)
}
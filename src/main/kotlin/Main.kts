val text = """#2021-07-15
0500 - 1700

#2021-07-14
0800 - 1100
1400 - 2300

#2021-07-13
0800 - 1200
1230 - 1700

#2021-07-12
0800 - 1100
1400 - 2300

#2021-07-11
0800 - 1210

#2021-07-10
1210 - 1500

#2021-07-09
1210 - 1220""".trimIndent()

val timesheet = text.toTimesheet()
val (normalizedTimesheet, delta) = timesheet.normalize()

println(delta)
println(normalizedTimesheet)


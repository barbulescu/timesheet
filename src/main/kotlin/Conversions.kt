import kotlin.math.roundToInt

fun hoursToMinutes(hours: Double): Int = (hours * 60).roundToInt()
import kotlinx.datetime.LocalDate
import kotlinx.datetime.toLocalDate
import java.time.DayOfWeek

data class Day(private val date: LocalDate, private val intervals: List<Interval>) {

    constructor(date: LocalDate, vararg intervals: Interval) : this(date, intervals.toList())

    override fun toString(): String {
        val stringIntervals = intervals.joinToString(separator = "\n") { it.toString() }
        return """
            |$date
            |$stringIntervals
        """.trimMargin()
    }

    fun valid() = intervals.isNotEmpty()

    fun totalMinutes() = intervals.sumOf(Interval::minutes)

    fun normalize() = normalizedDay(date)
    fun isWeekend(): Boolean = date.dayOfWeek == DayOfWeek.SATURDAY || date.dayOfWeek == DayOfWeek.SUNDAY

    companion object {
        fun normalizedDay(date: LocalDate) = Day(date, "0800 - 1200".toInterval(), "1230 - 16:54".toInterval())
    }
}

fun String.toDay(): Day {
    val lines = this.split("\n")
        .filterNot(String::isBlank)
    val date = lines.first().toLocalDate()
    val intervals = lines.drop(1)
        .map(String::toInterval)
    return Day(date, intervals)
}
